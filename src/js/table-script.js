let DATA = [
    {name: "Товар 1", count: 5, price: 12352.25},
    {name: "Товар 2", count: 5, price: 11352.25},
    {name: "Товар 3", count: 5, price: 13352.25}
];

let IDS = [];

function update_ids() {
    IDS = [];
    for(let i = 0; i < DATA.length; i++)
        IDS.push(i);
}

$(document).ready(function () {
    update_ids();
    fill_table();
    clear_form();
});

function fill_table() {
    let tbody = $('#items-table-body');
    tbody.empty();
    IDS.forEach(function (item) {
        tbody.append($('<tr>')
            .append($('<td>')
                .addClass('align-middle')
                .append($('<span>')
                    .attr('id', 'name' + item)
                    .append(DATA[item]['name']))
                .append($('<span>')
                    .attr('id', 'count' + item)
                    .attr('style', 'line-height: unset')
                    .addClass('badge badge-secondary badge-pill float-right')
                    .append(DATA[item]['count'])))
            .append($('<td>')
                .addClass('align-middle text-center')
                .attr('id', 'price' + item)
                .append(convert_price_to_str(DATA[item]['price'])))
            .append($('<td>')
                .addClass('align-middle text-center')
                .append($('<button>')
                    .addClass('btn table-button')
                    .append('Edit')
                    .click(function () {
                        fill_form(item);
                        $('#add-update-modal').modal('show');
                    }))
                .append($('<button>')
                    .addClass('btn')
                    .append('Delete')
                    .click(function () {
                        $('#id-delete-field').val(item);
                        $('#delete-modal').modal('show');
                    }))))
    });
}

function clear_form() {
    $('#id-field').val(DATA.length);
    $('#name-field').val('').removeClass('error-field');
    $('#count-field').val('').removeClass('error-field');
    $('#price-num-field').val('');
    $('#price-str-field').val('').removeClass('error-field');
    $('#form-btn').empty().append('Add');
    $('#name-error').empty();
    $('#count-error-error').empty();
    $('#price-error').empty();
}

function convert_price_to_str(price) {
    if (price === "")
        return "";
    let digits = [];
    while (price > 0) {
        digits.push(Math.round((price % 1000) * 100) / 100);
        price = Math.floor(price / 1000);
    }
    return '$' + digits.reverse().join(',');
}

function fill_form(id) {
    let item = DATA[id];
    $('#id-field').val(id);
    $('#name-field').val(item['name']).removeClass('error-field');
    $('#count-field').val(item['count']).removeClass('error-field');
    $('#price-num-field').val(item['price']);
    $('#price-str-field').val(convert_price_to_str(item['price'])).removeClass('error-field');
    $('#form-btn').empty().append('Update');
    $('#name-error').empty();
    $('#count-error-error').empty();
    $('#price-error').empty();
    change_price();
    change_count();
}

function filter_data() {
    update_ids();
    clear_sorts();
    let filter_string = $('#filter-input').val().trim().toLowerCase();
    if (filter_string === "") {
        fill_table();
        return;
    }
    IDS = IDS.filter(item => ~DATA[item]['name'].trim().toLowerCase().indexOf(filter_string));
    fill_table();
}

function focus_price() {
    $('#price-str-field').hide();
    $('#price-num-field').show().focus();
    $('#price-str-label').hide();
    $('#price-num-label').show();
}

function lose_focus_price() {
    const str_field = $('#price-num-field');
    $('#price-str-field').show().val(convert_price_to_str(str_field.val()));
    str_field.hide();
    $('#price-str-label').show();
    $('#price-num-label').hide();
    check_price();
}

function check_name() {
    let error = $('#name-error'),
        field = $('#name-field'),
        name = field.val().trim(),
        empty_str = (name.length === 0),
        long_str = (name.length > 15);
    error.empty();
    if (!long_str && !empty_str) {
        field.removeClass('error-field');
        return true;
    }
    if (empty_str) {
        error.append(" • Required field").show();
    } else if (long_str) {
        error.append(" • Less than 15 symbols").show();
    } else {
        error.append(" • Uncommon error").show();
    }
    field.addClass('error-field');
    return false;
}

function check_count() {
    let error = $('#count-error'),
        field = $('#count-field'),
        count = field.val().trim();
    error.empty();
    if ((new RegExp('^\\d*$')).test(count)) {
        field.removeClass('error-field');
        return true;
    }
    error.append(" • Only digits are available").show();
    field.addClass('error-field');
    return false;
}

function check_price() {
    let error = $('#price-error'),
        field = $('#price-str-field'),
        price = $('#price-num-field').val().trim();
    error.empty();
    if (new RegExp('^\\d*\\.?\\d{0,2}$').test(price)) {
        field.removeClass('error-field');
        return true;
    }
    error.append(" • Only numbers are available").show();
    field.addClass('error-field');
    return false;
}

function change_count() {
    $('#count-field').val(function (index, value) {
        return value.replace(new RegExp('\\D+', 'g'), '');
    });
}

function change_price() {
    $('#price-num-field').val(function (index, value) {
        let digits_and_dots = value.replace(new RegExp('[^0-9\\.]+', 'g'), '');
        return digits_and_dots.replace(new RegExp('^(\\d*\\.?\\d{0,2}).*$'), '$1');
    });
}

function is_correct() {
    let name_ok = check_name(),
        count_ok = check_count(),
        price_ok = check_price();
    return name_ok && count_ok && price_ok;
}

function add_data() {
    if (is_correct()) {
        let id = $('#id-field').val();
        DATA[id] = {
            name: $('#name-field').val(),
            count: $('#count-field').val(),
            price: $('#price-num-field').val()
        };
        filter_data();
        clear_form();
        $('#add-update-modal').modal('hide')
    }
}

function delete_data() {
    DATA.splice(($('#id-delete-field').val()), 1);
    filter_data();
    clear_form();
}

//sorting functions
function clear_sorts() {
    const sortable_fields = ['name', 'price'];
    sortable_fields.forEach(function (value) {
        $('#' + value + '-sort-btn').show();
        $('#' + value + '-sort-up-btn').hide();
        $('#' + value + '-sort-down-btn').hide();
    });
}

function sort(field) {
    clear_sorts();
    $('#' + field + '-sort-btn').hide();
    sort_dir(field, true)
}

function sort_dir(field, upgrade) {
    let dir, dir_reverse;

    if (upgrade) {
        dir = "up";
        dir_reverse = "down";
    }
    else {
        dir = "down";
        dir_reverse = "up";
    }
    $('#' + field + '-sort-' + dir_reverse + '-btn').hide();
    $('#' + field + '-sort-' + dir + '-btn').show();
    IDS.sort(function (a, b) {
        if (DATA[a][field] === DATA[b][field])
            return 0;
        if (DATA[a][field] < DATA[b][field]) {
            if (upgrade)
                return -1;
            else
                return 1;
        }
        if (DATA[a][field] > DATA[b][field]){
            if (upgrade)
                return 1;
            else
                return -1;
        }
    });
    fill_table();
}